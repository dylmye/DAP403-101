import os
import unittest

from .. import app


class ConverterTests(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def tearDown(self):
        pass

    def test_pages_render(self):
        result = self.app.get('/')
        self.assertEqual(result.status_code, 200)

    def test_nonexistent_page_returns_404(self):
        result = self.app.get('/missing-page-that-doesnt-exist')
        self.assertEqual(result.status_code, 404)

    def test_currency_converter_calculates_correctly(self):
        # assumptions: GBP-USD rate is 1.39945
        # GBP-USD@1.39945: £15.00 is US$20.99175
        result = self.app.get('/get-converted-amount?amount=15&from=GBP&to=USD')
        self.assertEqual(result.data, b'20.99')
