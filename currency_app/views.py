# -*- coding: utf-8 -*-

from flask import render_template, request, redirect, make_response
from currency_app import app
from io import BytesIO
from matplotlib import rcParams
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

### matplotlib setup
rcParams.update({
    'font.family': 'Roboto, Helvetica Neue, Arial, sans-serif',
    'font.size': 16
})

### Constants

currency_rates_readonly = {
    "GBP": {
        "USD": 1.39945,
        "CAD": 1.83293,
        "EUR": 1.14078
    },
    "USD": {
        "GBP": 0.714561,
        "CAD": 1.30926,
        "EUR": 0.814823
    },
    "CAD": {
        "GBP": 0.545575,
        "USD": 0.763790,
        "EUR": 0.622467
    },
    "EUR": {
        "GBP": 0.876594,
        "USD": 1.22726,
        "CAD": 1.60651
    }
}

current = "GBP"

isAdmin = False

### Helper Functions


def get_available_currencies(currency_to_remove):
    """Creates a list of currencies available to pair/trade with the current    
    Args:
        - currency_to_remove: currency: string
    Returns:
        - currency_rates: list
    """

    currency_rates = dict(currency_rates_readonly) # make a shallow copy of the ro object
    del currency_rates[currency_to_remove]  # remove current currency
    return currency_rates

### View Requests


@app.route("/")
def viewIndex():
    """Displays the index page    
    Returns:
        - RenderedTemplate
    """
    return render_template('index.html', current_currency=current, currency_options=get_available_currencies(current), raw_currency_options=currency_rates_readonly, logged_in=isAdmin)


@app.route("/buy")
def viewBuy():
    """Displays the Buy Page for a given currency pair    
    Args:
        - to: currency: string
    Returns:
        - RenderedTemplate
    """
    params = {
        'to': request.args.get('to')
    }
    if not params["to"] or not params["to"] in currency_rates_readonly: # validation: ensure a valid currency pair is passed
        return redirect("/")

    return render_template('buy.html', result_currency=params["to"], rate=currency_rates_readonly[current][params["to"]], current_currency=current, currency_options=get_available_currencies(current), logged_in=isAdmin)


@app.route("/sell")
def viewSell():
    """Displays the Sell Page for a given currency pair    
    Args:
        - to: currency: string
    Returns:
        - RenderedTemplate
    """
    params = {
        'to': request.args.get('to')
    }
    if not params["to"] or not params["to"] in currency_rates_readonly: # validation: ensure a valid currency pair is passed
        return redirect("/")

    return render_template('sell.html', result_currency=params["to"], rate=currency_rates_readonly[params["to"]][current], current_currency=current, currency_options=get_available_currencies(current), logged_in=isAdmin)

### Value Setting Requests


@app.route("/change-current")
def setCurrentCurrency():
    """Takes a currency string and sets that as the current default currency which is used to compare currency to.
    Returns a redirect to the url set in the return parameter.    
    Args:
        - new: currency: string
        - return: url: string
    Returns:
        - redirect
    """
    params = {
        'new': request.args.get("new"),
        'return': request.args.get("return") if request.args.get("return") else "#"
    }
    if params["new"] is None: # validation: make sure all parameters have a value
        return redirect(params["return"])

    global current

    current = params["new"]
    return redirect(params["return"])


@app.route("/update-rate")
def setSelectedRate():
    """Sets a pair rate if the user is an admin   
    Args:
        - amount: float
        - new: currency: string
        - return: url: string
    Returns:
        - redirect
    """
    params = {
        'amount': request.args.get("amount"),
        'from': request.args.get("from"),
        'to': request.args.get("to"),
        'return': request.args.get("return") if request.args.get("return") else "#"
    }
    if None in params.values() or not params["to"] in currency_rates_readonly or not params["from"] in currency_rates_readonly: # validation: make sure all parameters have a value
        return redirect(params["return"])

    global isAdmin
    if not isAdmin:
        return redirect(params["return"])

    currency_rates_readonly[params["from"]][params["to"]] = float(params["amount"])
    return redirect(params["return"])


@app.route("/login")
def loginAsAdmin():
    """Checks that an admin credential is provided. If it is, sets a flag to indicate logged in state.    
    Args:
        - pwd: string
        - return: url: string
    Returns:
        - redirect
    """
    params = {
        'pwd': request.args.get("pwd"),
        'return': request.args.get("return") if request.args.get("return") else "#"
    }
    if params["pwd"] is None: # validation: make sure all parameters have a value
        return redirect(params["return"])

    global isAdmin

    if params["pwd"] == "fx999": 
        isAdmin = True
    else:
        isAdmin = False
    return redirect(params["return"])


@app.route("/logout")
def logoutUser():
    """Sets the logged in flag to the default value.    
    Args:
        - return: url: string
    Returns:
        - redirect
    """
    params = {
        'return': request.args.get("return") if request.args.get("return") else "#"
    }
    global isAdmin

    isAdmin = False
    return redirect(params["return"])

### Value Getting Requests


@app.route("/get-converted-amount")
def getConvertedAmount():
    """Takes a currency amount (in the `from` currency) and returns the amount in the `to` currency.    
        Args:
            - amount: float
            - from: currency: string
            - to: currency: string
        Returns:
            - convertedAmount: int
    """
    params = {
        'amount': request.args.get("amount"),
        'from': request.args.get("from"),
        'to': request.args.get("to")
    }
    if None in params.values(): # validation: make sure all parameters have a value
        return '0'

    rate = currency_rates_readonly[params["from"]][params["to"]]
    converted = float(params["amount"]) * rate # the converted amount is original_amount * rate
    return '{:.2f}'.format(converted) # format the output to 2 significant figures, as expected by users

### Graphing Requests


@app.route("/buy-sell-compare.png")
def graphBuySellRates():
    """Creates a currency pair rate comparison bar graph    
        Args:
            - from: currency: string
            - to: currency: string
        Returns:
            - response
    """
    params = {
        'from': request.args.get("from"),
        'to': request.args.get("to")
    }
    if None in params.values(): # validation: make sure all parameters have a value
        return redirect("#")

    buyRate = currency_rates_readonly[params["from"]][params["to"]]
    sellRate = currency_rates_readonly[params["to"]][params["from"]]

    figure = plt.figure() # set up our graph

    rateData = {u'Buy Rate': buyRate, u'Sell Rate': sellRate} # create data to add to our graph

    plt.bar(range(2), rateData.values(), color='#0D47A1') # add the rates as bars on the graph
    plt.xticks(range(2), rateData.keys()) # add the keys to show under the bars
    plt.title(params["from"] + " to/from " + params["to"] + " rates") # create a title so it can be saved

    canvas = FigureCanvas(figure) # add figure to a raster canvas so it can be saved
    buffer = BytesIO() # create a memory buffer to store the raster image
    canvas.print_png(buffer) # output the canvas onto the buffer

    response = make_response(buffer.getvalue()) # get the cavas output from the buffer and serve it
    response.mimetype = 'image/png'
    return response

### Error handling


@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404
