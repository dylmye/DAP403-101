# dap403-fx
Currency Exchange flask app. [DAP403-CHI]

![gitlab build pipeline status](https://gitlab.com/dylmye/DAP403-101/badges/master/build.svg)

### to install and set up

I recommend setting up a virtual environment first (your python scripts folder will need to be in PATH, and you need to run this from the project root):    
```
C:\dap403-fx $ pip install virtualenv
> [...] Successfully installed virtualenv
C:\dap403-fx $ virtualenv --version
> 15.1.0
C:\dap403-fx $ virtualenv dap403fx
> [...] done.
C:\dap403-fx $ .\env\Scripts\activate
> (dap403fx)
```    
`pip install -r requirements.txt` in the project root.

### to run

`python run.py` in the project root.

To log in (and enable rate changing), you need to click the user profile icon in the top right. The admin password is `fx999`. 

To change the currency you buy from, click the "GBP" at the top right of the page. This is your "default" currency.

To buy or sell a currency pair, click the appropriate button on the home page. 

To change a rate, log in as admin and navigate to a buy/sell page. Entering the new value and clicking "submit" will update the rate.

### to test

`nose2` to run tests using nose2. You need to install and set up the project beforehand.

You can also check for code that goes against best practices. To do this, after installing and setting up the project, run `flake8 path\to\file.py --ignore E266,E501,E261,W291` with "path\to\file.py" being the path to the file then the filename. e.g:

```
C:\dap403-fx\currency_app $ flake8 __init__.py --ignore E266,E501,E261,W291
> [all of the issues here]
```