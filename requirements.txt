Flask==0.12
flake8==3.4.1
nose2==0.7.4
numpy==1.14.2
matplotlib==2.2.2